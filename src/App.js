// import React, { useState } from 'react';
// import ModalButton from './Components/Batton/Batton';
// import ModalWindow from './Components/Modal/ModalWindow';
// import "../src/App.scss"



// const App = () => {

//   const [openFirstModal, setOpenFirstModal] = useState(false); /* setOpenFirstModal змінює булуве значення для змінної openFirstModal */
//   const [openSecondModal, setOpenSecondModal] = useState(false); /* те саме */


//   const handleButtonClickOne = () => {
//     setOpenFirstModal(true);
//   };

//   const handleButtonClickTwo = () => {
//     setOpenSecondModal(true);
//   };

//   const closeWindowClickOne = () => {
//     setOpenFirstModal(false);
//   };

//   const closeWindowClickTwo = () => {
//     setOpenSecondModal(false);
//   };

//   return (
//     <div className='mainContainer'>
//       <div>
//         <div>

//           <ModalButton
//             backgroundColor="blue"
//             text="Open first modal"
//             onClick={handleButtonClickOne}
//           />
//           <ModalButton
//             backgroundColor="red"
//             text="Open second modal"
//             onClick={handleButtonClickTwo}
//           />

//         </div>


//         <div>

//           {openFirstModal && <ModalWindow /* якщо openFirstModal true - модал вікно рендериться */
//             overModalClose={closeWindowClickOne}
//             handleClose={closeWindowClickOne}
//             header="My First Modal Window"
//             closeButton={true}
//             text="This is the content of the modal 1."
//             actions={
//               <div className='buttonsContainer'>
//                 <button onClick={closeWindowClickOne}>OK</button>
//                 <button onClick={closeWindowClickOne}>Cancel</button>
//               </div>
//             }
//           />}

//           {openSecondModal && <ModalWindow /* якщо openSecondModal true - модал вікно рендериться */
//             overModalClose={closeWindowClickTwo}
//             handleClose={closeWindowClickTwo}
//             header="My Second Modal Window"
//             closeButton={true}
//             text="This is the content of the modal 2."
//             actions={
//               <div className='buttonsContainer'>
//                 <button onClick={closeWindowClickTwo}>OK</button>
//                 <button onClick={closeWindowClickTwo}>Cancel</button>
//               </div>
//             }
//           />}
//         </div>
//       </div>

//     </div>


//   );
// };

// export default App;

import React, { useState } from 'react';
import ModalButton from './Components/Batton/Batton';
import ModalWindow from './Components/Modal/ModalWindow';
import "../src/App.scss";

const App = () => {
  const [openModal, setOpenModal] = useState(null);

  const handleButtonClick = (modalNumber) => {
    setOpenModal(modalNumber);
  };

  const handleCloseModal = () => {
    setOpenModal(null);
  };

  return (
    <div className='mainContainer'>
      <div>
        <div>
          <ModalButton
            backgroundColor="blue"
            text="Open first modal"
            onClick={() => handleButtonClick(1)}
          />
          <ModalButton
            backgroundColor="red"
            text="Open second modal"
            onClick={() => handleButtonClick(2)}
          />
        </div>

        <div>
          {openModal === 1 && (
            <ModalWindow
              overModalClose={handleCloseModal}
              handleClose={handleCloseModal}
              header="My First Modal Window"
              closeButton={true}
              text="This is the content of the modal 1."
              actions={
                <div className='buttonsContainer'>
                  <button onClick={handleCloseModal}>OK</button>
                  <button onClick={handleCloseModal}>Cancel</button>
                </div>
              }
            />
          )}

          {openModal === 2 && (
            <ModalWindow
              overModalClose={handleCloseModal}
              handleClose={handleCloseModal}
              header="My Second Modal Window"
              closeButton={true}
              text="This is the content of the modal 2."
              actions={
                <div className='buttonsContainer'>
                  <button onClick={handleCloseModal}>OK</button>
                  <button onClick={handleCloseModal}>Cancel</button>
                </div>
              }
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default App;

