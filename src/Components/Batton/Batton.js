import "./Batton.scss"

const ModalButton = ({ backgroundColor, text, onClick }) => {
  const modalStyle = {
    backgroundColor: backgroundColor || 'green',
   
  };

  return (
    <button className="modalButton" style={modalStyle} onClick={onClick}>
      {text}
    </button>
  );
};


export default ModalButton;