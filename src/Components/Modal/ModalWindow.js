import React from 'react';
import "./ModalWindow.scss"


const ModalWindow = ({ header, closeButton, text, actions, handleClose, overModalClose }) => {
  

  return (
    <div className="modalOverlay" onClick={overModalClose}>
      <div className="modalContent" onClick={(e) => e.stopPropagation()}>
        {closeButton && (
          <span className="closeButton" onClick={handleClose}>
            &times;
          </span>
        )}
        {header && <h2>{header}</h2>}
        <div className="modalText">{text}</div>
        <div className="modalActions">{actions}</div>
      </div>
    </div>
  );
};


export default ModalWindow;